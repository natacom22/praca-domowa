#!/usr/bin/env python3
import random

counter = 10

while counter:
    print(counter, end=" ")
    counter -= 1

print()
counter = 10

while counter <= 20:
    print(counter, end=" ")
    counter += 1

print()
counter = 0

while counter != 15:
    counter = random.randrange(10, 20)
    print(counter, end=" ")